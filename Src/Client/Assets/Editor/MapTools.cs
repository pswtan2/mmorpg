﻿using Common.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MapTools {

    [MenuItem("Map Tools/Export Teleporters")]//定义一个unity的菜单项
    public static void ExportTeleporters()
    {
        DataManager.Instance.Load();//初始化Data目录

        Scene current = EditorSceneManager.GetActiveScene();
        string currentScene = current.name;//获取当前场景是什么
        if (current.isDirty)//检查场景是否保存
        {
            EditorUtility.DisplayDialog("提示", "请先保存当前场景", "确定");
            return;
        }

        List<TeleporterObject> allTeleporters = new List<TeleporterObject>();

        foreach (var map in DataManager.Instance.Maps)//遍历当前地图表
        {
            string sceneFile = "Assets/Levels/" + map.Value.Resource + ".unity";//根据地图配置名字生成原始路径
            if (!System.IO.File.Exists(sceneFile))//判断场景是否存在
            {
                Debug.LogWarningFormat("Scene {0} not exist", sceneFile);
                continue;
            }
            EditorSceneManager.OpenScene(sceneFile, OpenSceneMode.Single);

            TeleporterObject[] teleporters = GameObject.FindObjectsOfType<TeleporterObject>();//返回场景中所有传送点类型的Object为一个list
            foreach (var teleporter in teleporters)
            {
                if(!DataManager.Instance.Teleporters.ContainsKey(teleporter.ID))
                {
                    EditorUtility.DisplayDialog("错误", string.Format("地图: {0} 中配置的 Teleporter: [{1}] 不存在", map.Value.Resource, teleporter.ID),"确定");
                    return;
                }

                TeleporterDefine def = DataManager.Instance.Teleporters[teleporter.ID];
                if (def.MapID != map.Value.ID)
                {
                    EditorUtility.DisplayDialog("错误", string.Format("地图: {0} 中配置的 Teleporter: [{1}] 错误 MapID: {2} 错误", map.Value.Resource, teleporter.ID, map.Value.ID), "确定");
                    return;
                }
                def.Position = GameObjectTool.WorldToLogicN(teleporter.transform.position);//把世界坐标转换成逻辑坐标
                def.Direction = GameObjectTool.WorldToLogicN(teleporter.transform.forward);
            }
            
        }
        DataManager.Instance.SaveTeleporters();
        EditorSceneManager.OpenScene("Assets/Levels/" + currentScene + ".unity");
        EditorUtility.DisplayDialog("提示", "传送点导出完成", "确定");
    }
    
}
