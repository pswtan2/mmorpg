﻿using Entities;
using SkillBridge.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Managers
{
    interface IEntityNotify
    {
        void OnEntityRemoved();
        void OnEntityChanged(Entity entity);
        void OnEntityEvent(EntityEvent @event);
    }
    class EntityManager : Singleton<EntityManager>
    {
        Dictionary<int, Entity> entities = new Dictionary<int, Entity>();

        Dictionary<int, IEntityNotify> notifies = new Dictionary<int, IEntityNotify>();

        public void RegisterEntityChangeNotify(int entityId, IEntityNotify notify)
        {
            this.notifies[entityId] = notify;
        }
        public void AddEntity(Entity entity)
        {
            entities[entity.entityId] = entity;
        }

        public void RemoveEntity(NEntity entity)
        {
            this.entities.Remove(entity.Id);
            if (notifies.ContainsKey(entity.Id))
            {
                notifies[entity.Id].OnEntityRemoved();
                notifies.Remove(entity.Id);
            }

        }

        internal void OnEntitySync(NEntitySync otherEntity)
        {
            Entity entity = null;
            entities.TryGetValue(otherEntity.Id, out entity);//搜索其他玩家的entityId是否存在,若存在则将这个其他玩家id对应的entity赋值给当前entity
            if (entity != null)
            {
                if (otherEntity != null)
                {
                    entity.EntityData = otherEntity.Entity;
                }
                if (notifies.ContainsKey(otherEntity.Id))
                {
                    notifies[entity.entityId].OnEntityChanged(entity);
                    notifies[entity.entityId].OnEntityEvent(otherEntity.Event);
                }
            }
        }
    }
}
