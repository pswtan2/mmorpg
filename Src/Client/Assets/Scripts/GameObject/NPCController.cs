﻿using Managers;
using Common.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Models;

public class NPCController : MonoBehaviour {

    public int npcID;

    SkinnedMeshRenderer renderer;
    Animator anim;
    Color originalColor;

    private bool interactive = false;

    NpcDefine npc;
	// Use this for initialization
	void Start () {
        renderer = this.gameObject.GetComponentInChildren<SkinnedMeshRenderer>();
        anim = this.gameObject.GetComponent<Animator>();
        originalColor = renderer.sharedMaterial.color;//设置材质的颜色
        npc = NPCManager.Instance.GetNpcDefine(npcID);
        this.StartCoroutine(Actions());
	}

    IEnumerator Actions()
    {
        while (true)
        {
            if (interactive)
            {
                yield return new WaitForSeconds(2);
            }
            else
            {
                yield return new WaitForSeconds(UnityEngine.Random.Range(5f, 10f));
                this.Relax();
                yield return new WaitForSeconds(2);
                anim.SetTrigger("Idle");

            }
                
            
        }
    }

    void Relax()
    {
        anim.SetTrigger("Relax");
        
    }

	// Update is called once per frame
	void Update () {
		
	}

    void Interactive()
    {
        if (!interactive)
        {
            interactive = true;
            StartCoroutine(DoInteractive());
        }
    }

    IEnumerator DoInteractive()
    {
        yield return FaceToPlayer();
        if (NPCManager.Instance.Interactive(npc))
        {
            anim.SetTrigger("Talk");
        }
        yield return new WaitForSeconds(3f);
        interactive = false;
    }

    IEnumerator FaceToPlayer()
    {
        Vector3 faceTo = (User.Instance.CurrentCharacterObject.transform.position - this.gameObject.transform.position).normalized;
        if (Mathf.Abs(Vector3.Angle(this.gameObject.transform.forward,faceTo)) > 5f)
        {
            this.gameObject.transform.forward = Vector3.Lerp(this.gameObject.transform.forward, faceTo, Time.deltaTime * 0.5f);
            yield return null;
        }
    }

    private void OnMouseDown()
    {
        Interactive();
    }

    private void OnMouseOver()
    {
        Highlight(true);
    }

    private void OnMouseEnter()
    {
        Highlight(true);
    }

    private void OnMouseExit()
    {
        Highlight(false);
    }

    void Highlight(bool hightlight)
    {
        if (hightlight)
        {
            if (renderer.sharedMaterial.color != Color.white)
            {
                renderer.sharedMaterial.color = Color.white;
            }
            
        }
        else if(renderer.sharedMaterial.color != originalColor)
        {
            renderer.sharedMaterial.color = originalColor;
        }
        
    }
}
