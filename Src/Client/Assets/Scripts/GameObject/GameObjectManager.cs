﻿using Managers;
using Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Models;

public class GameObjectManager : MonoSingleton<GameObjectManager>//使用单例保证场景切换时角色不删除
{

    Dictionary<int, GameObject> Characters = new Dictionary<int, GameObject>();
    //初始化
    protected override void OnStart()//使用单例类需要重载Start(必须这么做)
    {
        StartCoroutine(InitGameObjects());
        CharacterManager.Instance.OnCharacterEnter += OnCharacterEnter;//使用委托尽量成对出现 即+= 和 -=
        CharacterManager.Instance.OnCharacterLeave += OnCharacterLeave;
    }
    private void OnDestroy()
    {
        CharacterManager.Instance.OnCharacterEnter -= OnCharacterEnter;
        CharacterManager.Instance.OnCharacterLeave -= OnCharacterLeave;
    }

    private void OnCharacterEnter(Character cha)
    {
        CreateCharacterObject(cha);
    }
    private void OnCharacterLeave(Character character)
    {
        if (!this.Characters.ContainsKey(character.entityId))//entityId是内存中出现标识唯一性的ID,尽量用entityId
            return;
        
            
        if (Characters[character.entityId] != null)
        {
            Destroy(Characters[character.entityId]);//删除游戏中的角色
            this.Characters.Remove(character.entityId);//在列表中删除对象
        }
        
        
    }
    IEnumerator InitGameObjects()
    {
        foreach (var cha in CharacterManager.Instance.Characters.Values)
        {
            CreateCharacterObject(cha);
            yield return null;
        }
    }

    private void CreateCharacterObject(Character character)
    {
        if (!Characters.ContainsKey(character.entityId) || Characters[character.entityId] == null)//如果当前Characters不存在进入的游戏id 或者 进入的角色Id没有对应的对象
        {
            UnityEngine.Object obj = Resloader.Load<UnityEngine.Object>(character.Define.Resource);
            if (obj == null)
            {
                Debug.LogErrorFormat("Character[{0}] Resource[{1}] not existed", character.Define.TID, character.Define.Resource);
                return;
            }

            GameObject go = (GameObject)Instantiate(obj, this.transform);
            go.name = "Character_" + character.Info.Id + "_" + character.Info.Name;
            Characters[character.entityId] = go;
            
            UIWorldElementManager.Instance.AddCharacterNameBar(go.transform, character);
        }
        this.InitGameObject(Characters[character.entityId], character);
    }

    private void InitGameObject(GameObject go, Character character)
    {
        go.transform.position = GameObjectTool.LogicToWorld(character.position);
        go.transform.forward = GameObjectTool.LogicToWorld(character.direction);
        EntityController ec = go.GetComponent<EntityController>();
        if (ec != null)
        {
            ec.entity = character;
            ec.isPlayer = character.IsPlayer;
        }

        PlayerInputController pc = go.GetComponent<PlayerInputController>();
        if (pc != null)
        {
            if (character.Info.Id == Models.User.Instance.CurrentCharacter.Id)
            {
                User.Instance.CurrentCharacterObject = go;
                MainPlayerCamera.Instance.player = go;
                pc.enabled = true;
                pc.character = character;
                pc.entityController = ec;
            }
            else
            {
                pc.enabled = false;
            }
        }
        
    }
}
