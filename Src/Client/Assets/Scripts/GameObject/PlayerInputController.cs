﻿using Entities;
using Models;
using Services;
using SkillBridge.Message;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInputController : MonoBehaviour {

    public Rigidbody rb;

    CharacterState state;

    public Character character;

    public float rotateSpeed = 2.0f;

    public float turnAngle = 10;

    public int speed;

    public EntityController entityController;

    public bool onAir = false;

    private void Start()
    {
        state = SkillBridge.Message.CharacterState.Idle;
        if (this.character == null)
        {
            DataManager.Instance.Load();
            NCharacterInfo cInfo = new NCharacterInfo();
            cInfo.Id = 1;
            cInfo.Name = "Test";
            cInfo.Tid = 1;
            cInfo.Entity = new NEntity();
            cInfo.Entity.Position = new NVector3();
            cInfo.Entity.Direction = new NVector3();
            cInfo.Entity.Direction.X = 0;
            cInfo.Entity.Direction.Y = 100;
            cInfo.Entity.Direction.Z = 0;
            this.character = new Character(cInfo);

            if (entityController != null) entityController.entity = this.character;
        }
    }

    private void FixedUpdate()
    {
        if (character == null)
        {
            return;
        }

        float ver = Input.GetAxisRaw("Vertical");
        if (ver > 0.01)
        {
            if (state != CharacterState.Move)
            {
                state = CharacterState.Move;
                this.character.MoveForward();
                this.SendEntityEvent(EntityEvent.MoveFwd);
            }

            this.rb.velocity = this.rb.velocity.y * Vector3.up + GameObjectTool.LogicToWorld(character.direction) * (this.character.speed + 9.81f) / 100f;
        }
        else if (ver < -0.01)
        {
            if (state != CharacterState.Move)
            {
                state = CharacterState.Move;
                this.character.MoveBack();
                this.SendEntityEvent(EntityEvent.MoveBack);
            }

            this.rb.velocity = this.rb.velocity.y * Vector3.up + GameObjectTool.LogicToWorld(character.direction) * (this.character.speed + 9.81f) / 100f;
        }
        else
        {
            if (state != CharacterState.Idle)
            {
                state = CharacterState.Idle;
                this.rb.velocity = Vector3.zero;
                this.character.Stop();
                this.SendEntityEvent(EntityEvent.Idle);
            }
        }

        if (Input.GetButtonDown("Jump"))
        {
            this.SendEntityEvent(EntityEvent.Jump);
        }

        float hor = Input.GetAxisRaw("Horizontal");
        if (hor < -0.01 || hor > 0.01)
        {
            this.transform.Rotate(0, hor * rotateSpeed, 0);
            Vector3 dir = GameObjectTool.LogicToWorld(this.character.direction);
            Quaternion rot = new Quaternion();
            rot.SetFromToRotation(dir, this.transform.forward);//创建一个dir方向到this.transform.forward方向的旋转
            if (rot.eulerAngles.y > this.turnAngle && rot.eulerAngles.y < (360 - this.turnAngle))
            {
                character.SetDirection(GameObjectTool.WorldToLogic(this.transform.forward));
                rb.transform.forward = this.transform.forward;
                this.SendEntityEvent(EntityEvent.None);
            }
        }
    }
    Vector3 lastPos;
    float lastSync = 0;
    private void LateUpdate()
    {
        if (this.character == null) return;

        Vector3 offset = this.rb.transform.position - lastPos;
        this.speed = (int)(offset.magnitude * 100f / Time.deltaTime);
        this.lastPos = this.rb.transform.position;

        if ((GameObjectTool.WorldToLogic(this.rb.transform.position) - this.character.position).magnitude > 50)
        {
            this.character.SetPosition(GameObjectTool.WorldToLogic(this.rb.transform.position));
            this.SendEntityEvent(EntityEvent.None);
        }
        this.transform.position = this.rb.transform.position;
    }

    private void SendEntityEvent(EntityEvent Event)
    {
        if (entityController != null)
        {
            entityController.OnEntityEvent(Event);
        }
        MapService.Instance.SendMapEntitySync(Event, this.character.EntityData);
    }
}
