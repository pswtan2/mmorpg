﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITest : UIWindow
{
    public Text titleText;
	// Use this for initialization
	void Start () {
        titleText.text = "武器店";
	}

    public void SetTitle(string title)
    {
        titleText.text = title;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
