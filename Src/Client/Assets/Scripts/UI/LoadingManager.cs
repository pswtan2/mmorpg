﻿using Managers;
using Services;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingManager : MonoBehaviour {

    public GameObject UITips;
    public GameObject UILoading;
    public GameObject UILogin;

    public Slider progressBar;
    public Text progressText;


    IEnumerator Start()
    {
        log4net.Config.XmlConfigurator.ConfigureAndWatch(new System.IO.FileInfo("log4net.xml"));
        UnityLogger.Init();
        Common.Log.Init("Unity");
        Common.Log.Info("LoadingManager start");

        UITips.SetActive(true);
        UILoading.SetActive(false);
        UILogin.SetActive(false);
        yield return new WaitForSeconds(2);
        UITips.SetActive(false);
        UILoading.SetActive(true);

        yield return DataManager.Instance.LoadData();

        UserService.Instance.Init();
        MapService.Instance.Init();
        TestManager.Instance.Init();
        //加载基础设备
        //MapService.
        for (float i = 50; i < 100;)
        {
            i += Random.Range(0.1f, 0.3f);
            
            progressBar.value = (float)(i/100);
            progressText.text = ((int)i).ToString() + "%";
            yield return new WaitForEndOfFrame();
        }

        UILoading.SetActive(false);
        UILogin.SetActive(true);
        yield return null;
    }

    private void Update()
    {
        
    }
}
