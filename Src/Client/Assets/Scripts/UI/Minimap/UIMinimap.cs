﻿using Managers;
using Models;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMinimap : MonoBehaviour {
    public Collider minimapBoundingBox;
    public Image minimap;
    public Image arrow;
    public Text mapName;

    private Transform playerTransform; 
	// Use this for initialization
	void Start ()
    {
        MinimapManager.Instance.minimap = this;
 
        this.UpdateMap();
	}
    public void UpdateMap()//更新调取MInimapManager中的minimapBoundingBox和LoadCurrentMinimap
    {
        this.mapName.text = User.Instance.CurrentMapData.Name;
        this.minimap.overrideSprite = MinimapManager.Instance.LoadCurrentMinimap();
        
        this.minimap.SetNativeSize();
        this.minimap.transform.localPosition = Vector3.zero;
        this.minimapBoundingBox = MinimapManager.Instance.MinimapBoundingBox;
        this.playerTransform = null;//当地图改变了 游戏对象清空
    }
	
	// Update is called once per frame
	void Update () {
        if (playerTransform == null)
        {
            playerTransform = MinimapManager.Instance.PlayerTransform;
        }
        if (minimapBoundingBox == null || playerTransform == null) return;

        float realWidth = minimapBoundingBox.bounds.size.x;//Box的长
        float realHeight = minimapBoundingBox.bounds.size.z;//Box的宽

        float relationLocationX = playerTransform.position.x - minimapBoundingBox.bounds.min.x;//游戏对象和Box原点的相对位置
        float relationLocationY = playerTransform.position.z - minimapBoundingBox.bounds.min.z;

        float pivotX = relationLocationX / realWidth;
        float pivotY = relationLocationY / realHeight;
        
        this.minimap.rectTransform.pivot = new Vector2(pivotX, pivotY);
        this.minimap.rectTransform.localPosition = Vector2.zero;

        this.arrow.transform.eulerAngles = new Vector3(0, 0, -playerTransform.eulerAngles.y);
    }
}
