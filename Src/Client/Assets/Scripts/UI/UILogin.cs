﻿using Services;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using SkillBridge.Message;
public class UILogin : MonoBehaviour {
    public InputField inputAcc;
    public InputField inputPass;

    public Button Login;
    

	// Use this for initialization
	void Start () {
        UserService.Instance.OnLogin = Onlogin;
        
	}
    void Onlogin(Result result, string msg)
    {
        if (result == Result.Success)
        {
            MessageBox.Show(msg);
            //登陆成功，进入角色选择
            SceneManager.Instance.LoadScene("CharSelect");
        }
        else
            MessageBox.Show(msg);
    }
    // Update is called once per frame
    void Update () {
		
	}

    public void OnClickLogin()
    {
        if (string.IsNullOrEmpty(this.inputAcc.text))
        {
            MessageBox.Show("请输入账号");
            return;
        }
        if (string.IsNullOrEmpty(this.inputPass.text))
        {
            MessageBox.Show("请输入密码");
            return;
        }
        UserService.Instance.SendLogin(this.inputAcc.text, this.inputPass.text);
    }

    
}
