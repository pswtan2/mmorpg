﻿using Network;
using Services;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIRegister : MonoBehaviour {
    public InputField username;
    public InputField passward;
    public InputField passwordConfirm;
    public Button buttonRegister;

    // Use this for initialization
    void Start ()
    {
        UserService.Instance.OnRegister = this.OnRegister;
    }

    void OnRegister(SkillBridge.Message.Result result, string msg)
    {
        MessageBox.Show(msg);
    }
    //Update is called once per frame
    void Update () {
		
	}

    public void OnClickRegister()
    {
        if (string.IsNullOrEmpty(this.username.text))
        {
            MessageBox.Show("请输入账号");
            return;
        }
        if (string.IsNullOrEmpty(this.passward.text))
        {
            MessageBox.Show("请输入密码");
            return;
        }
        if (string.IsNullOrEmpty(this.passwordConfirm.text))
        {
            MessageBox.Show("请确认密码");
            return;
        }
        if (this.passward.text != this.passwordConfirm.text)
        {
            MessageBox.Show("两次输入的密码不一致");
            return;
        }
        
        UserService.Instance.SendRegister(this.username.text, this.passward.text);
    }
    
}
